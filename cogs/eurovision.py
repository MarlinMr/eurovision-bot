import discord
from datetime import date
from discord.ext import commands
from collections import Counter
from os.path import expanduser
import filemanager
from variables import *
from requests import get

embedColour = 0x50bdfe

votelist    = [12, 10, 8, 7, 6, 5, 4, 3, 2, 1, 0]
configdir   = "/home/marlinmr/src/marlinmr/eurovision-bot/config/"
logdir      = "/home/marlinmr/src/marlinmr/eurovision-bot/logs/"
year        = str(date.today().year)
basefile    = year+"/"+episode

pointsfile  = filemanager.JsonLoader(configdir+basefile+".json")
logfile     = filemanager.FileLoader(logdir+basefile+".log")
logfile_vote = filemanager.FileLoader(logdir+basefile+"-votes.log")
countries   = filemanager.JsonLoader(configdir+"countries.json")

def invert(d): return d.__class__(map(reversed, d.items()))

def log(time, user, message, guild, channel, file):
    with file.as_file('a+') as f:
        f.write(str(time) + " " + str(user) + " " +
                str(message) + " " + str(guild) + " " + str(channel)+"\n")
    print(time, user, guild, message, channel)

def standings():
    with pointsfile.as_dict(write=True) as data:
        count = Counter({})
        for (_, voter) in data.items():
            inverted = invert(voter)
            inverted = {k: int(v) for k, v in inverted.items()}
            count += Counter(inverted)
    return(count)

def recursive_insert(d, points, country):
    points = str(points)
    try:
        idx = votelist.index(int(points))
        next_points = votelist[idx+1]
    except IndexError:
        return
    next_country = d.get(points)
    if next_country:
        d[points] = country
        recursive_insert(d, next_points, next_country)
    else:
        d[points] = country

def votes(points, country, author, time):
    with pointsfile.as_dict(write=True) as data:
        authordata = data[str(author)]

        # Filter current country
        for p, c in authordata.copy().items():
            if c == country:
                authordata.pop(p)

        recursive_insert(authordata, points, country)

    with logfile.as_file('a+') as f:
        f.write(str(time) + " " + str(standings()) + "\n")
    return(str(points) + " points to " + country)

class eurovision(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def eurohelp(self, ctx):
        """Prints helpfull message"""
        if ctx.guild == None:
            name = None
        else:
            name = ctx.guild.name
        log(ctx.message.created_at, ctx.message.author, ctx.message.content, name, ctx.message.channel, logfile)
        await ctx.send("This bot keeps track of discord votes during the Eurovision Song Contest. You can only vote for 10 different countries. You can only give 12, 10, 8, 7, 6, 5, 4, 3, 2, 1, or 0 points. You can change your votes at any time. Giving the same amount of points to the a new country, results in the newest vote pushing the older votes down the vote list. For syntaxt, ``€help eurovision``")

    @commands.command()
    async def songlist(self, ctx):
        """Lists all the songs of the current episode, in order"""
        if ctx.guild == None:
            name = None
        else:
            name = ctx.guild.name
        log(ctx.message.created_at, ctx.message.author, ctx.message.content, name, ctx.message.channel, logfile)
        desc = ""

        with countries.as_dict(write=False) as data:
            countryList = data

        if episode == "beta_sf1":
            countryIndex = sf1
            title="BETA: Eurovision 1st Semi-final"
        elif episode == "sf1":
            countryIndex = sf1
            title="Eurovision 1st Semi-final"
        elif episode == "sf2":
            countryIndex = sf2
            title="Eurovision 2nd Semi-final"
        elif episode == "finale":
            countryIndex = finale
            title="Eurovision final"

        for i in range(1,int((len(countryIndex)+1)/2)):
            desc = desc + "\n ***" + str(i) + "*** " + countryIndex[i] + " " + countryList[countryIndex[i]]["song"] + \
                " sung by " + countryList[countryIndex[i]]["artist"] + \
                " in " + countryList[countryIndex[i]]["lang"]
        embed = discord.Embed(
            description=desc, title=title, color=embedColour)
        desc = ""
        for i in range(int((len(countryIndex)+1)/2),len(countryIndex)+1):
            desc = desc + "\n ***" + str(i) + "*** " + countryIndex[i] + " " + countryList[countryIndex[i]]["song"] + \
                " sung by " + countryList[countryIndex[i]]["artist"] + \
                " in " + countryList[countryIndex[i]]["lang"]
        embed2 = discord.Embed(
            description=desc, title="", color=embedColour)
        await ctx.send(embed=embed)
        await ctx.send(embed=embed2)

    async def add_vote_reaction(self, ctx, points, country):
        esc = []
        if episode == "beta_sf1":
            countryIndex = sf1
            for ctry in sf1:
                esc.append(sf1[ctry])
        elif episode == "sf1":
            countryIndex = sf1
            for ctry in sf1:
                esc.append(sf1[ctry])
        elif episode == "sf2":
            countryIndex = sf2
            for ctry in sf2:
                esc.append(sf2[ctry])
        elif episode == "finale":
            countryIndex = finale
            for ctry in finale:
                esc.append(finale[ctry])
        if episode == "beta_sf1":
            if country in esc:
                await ctx.message.add_reaction(country)
            elif country.isnumeric():
                await ctx.message.add_reaction(sf1[int(country)])
        elif episode == "sf1":
            if country in esc:
                await ctx.message.add_reaction(country)
            elif country.isnumeric():
                await ctx.message.add_reaction(sf1[int(country)])
        elif episode == "sf2":
            if country in esc:
                await ctx.message.add_reaction(country)
            elif country.isnumeric():
                await ctx.message.add_reaction(sf2[int(country)])
        elif episode == "finale":
            if country in esc:
                await ctx.message.add_reaction(country)
            elif country.isnumeric():
                await ctx.message.add_reaction(finale[int(country)])

    @commands.command()
    async def vote(self, ctx, points, country: commands.clean_content):
        """Votes <points> on <country>. <country> is either the flag emoji of the country, or the number. I.E. 12 points to the Netherlands is done by typing €vote 12 23 or €vote 12 🇳🇱. For a list of song numbers, use €songlist"""
        if ctx.guild == None:
            name = None
        else:
            name = ctx.guild.name
        log(ctx.message.created_at, ctx.message.author, ctx.message.content, name, ctx.message.channel, logfile_vote)
#        await ctx.send("Voting Closed")
#        return
## Determine Episode
        esc = []
        if episode == "beta_sf1":
            countryIndex = sf1
            for ctry in sf1:
                esc.append(sf1[ctry])
        elif episode == "sf1":
            countryIndex = sf1
            for ctry in sf1:
                esc.append(sf1[ctry])
        elif episode == "sf2":
            countryIndex = sf2
            for ctry in sf2:
                esc.append(sf2[ctry])
        elif episode == "finale":
            countryIndex = finale
            for ctry in finale:
                esc.append(finale[ctry])

        with pointsfile.as_dict(write=False) as d:
            data = d

        if str(ctx.message.author.id) not in data:
            #await ctx.send("You have not entered the voting. Please ``€enter`` first. (For GDPR reasons)")
            with pointsfile.as_dict(write=True) as data:
                data.update({str(ctx.message.author.id): {}})
        if int(points) not in votelist:
            await ctx.message.add_reaction("❌")
            #await ctx.send("You can only give 12, 10, 8, 7, 6, 5, 4, 3, 2, 1 or 0 as points.")
        elif country in esc:
            votes(points, country, ctx.message.author.id, ctx.message.created_at)
            await self.add_vote_reaction(ctx, points, country)
        elif (country.isnumeric()):
            try:
               # print("Country", country)
                if country.isnumeric():
                    if 0 < int(country) <= len(countryIndex):
                        votes(points, countryIndex[int(country)], ctx.message.author.id, ctx.message.created_at)
                        await self.add_vote_reaction(ctx, points, country)
                    else:
                        #await ctx.send("This country is not in this episode. You can only vote for " + str(esc))
#                        ctry_list = ''.join([f"{n}" for n in esc])
#                        embed = discord.Embed(description=ctry_list, color=embedColour)
#                        await ctx.send(embed=embed)
                        await ctx.message.add_reaction("❌")
                        await ctx.message.add_reaction("1️⃣")
                        await ctx.message.add_reaction("5️⃣")
                        #for ctry in esc:
                        #    await ctx.message.add_reaction(ctry)
            except Exception as e:
                if isinstance(country, str):
                    print(e)
                    #await ctx.send("This country is not in this episode. You can only vote for " + str(esc))
#                    ctry_list = ''.join([f"{n}" for n in esc])
#                    embed = discord.Embed(description=ctry_list, color=embedColour)
#                    await ctx.send(embed=embed)
                    await ctx.message.add_reaction("❌")
                    for country in esc:
                        await ctx.message.add_reaction(country)
                    #for ctry in esc:
                    #    await ctx.message.add_reaction(ctry)
                else:
                    await ctx.send("Voting error, contact <@202745416062599168>")
        #else:
        #    for cntry in country_names:
        #        if (country.lower() in cntry):
        #            print(cntry)
        else:
            for cntry in country_names:
                #print(country.lower(), country_names[cntry])
                if country.lower() in country_names[cntry]:
                    if cntry in esc:
                        votes(points, cntry, ctx.message.author.id, ctx.message.created_at)
                        await self.add_vote_reaction(ctx, points, cntry)
                    #votes(points, str(cntry), ctx.message.author.id, ctx.message.created_at)
                    #await self.add_vote_reaction(ctx, points, str(cntry))
                        return
            ctry_list = ''.join([f"{n}" for n in esc])
            #embed = discord.Embed(description=ctry_list, color=embedColour)
            await ctx.message.add_reaction("❌")
            for country in esc:
                await ctx.message.add_reaction(country)
            #await ctx.send(embed=embed)
            #await ctx.send("This country is not in this contest. You can only vote for " + str(esc))
            #for ctry in esc:
            #    await ctx.message.add_reaction(ctry)

    @commands.command()
    async def myVotes(self, ctx):
        """Lists your votes"""
        if ctx.guild == None:
            name = None
        else:
            name = ctx.guild.name
        log(ctx.message.created_at, ctx.message.author, ctx.message.content, name, ctx.message.channel, logfile)

        author_id = str(ctx.message.author.id)
        with pointsfile.as_dict(write=False) as data:
            if author_id in data:
                desc = ""
                for index in sorted(data[author_id].keys(), key=lambda k: int(k), reverse=True):
                    desc = desc + index + "  " + \
                        data[author_id][index] + ",\t\t"
                embed = discord.Embed(description=desc[:-3], color=embedColour)
                await ctx.send(embed=embed)
            else:
                #await ctx.send("You have not entered the voting. Please ``€enter`` first.")
                await ctx.message.add_reaction("❌")

    @commands.command()
    async def leaderboard(self, ctx):
        """Current leaderboard"""
        if ctx.guild == None:
            name = None
        else:
            name = ctx.guild.name
        log(ctx.message.created_at, ctx.message.author, ctx.message.content, name, ctx.message.channel, logfile)
        count = standings()
        desc = (
            ', '.join([f"{n} {i}" for i, n in count.most_common()]))
        if episode == "beta_sf1":
            embed = discord.Embed(description=desc, title="BETA: Eurovision 1st Semi-final", color=embedColour)
        elif episode == "sf1":
            embed = discord.Embed(description=desc, title="Eurovision 1st Semi-final", color=embedColour)
        elif episode == "sf2":
            embed = discord.Embed(description=desc, title="Eurovision 2nd Semi-final", color=embedColour)
        elif episode == "finale":
            embed = discord.Embed(description=desc, title="Eurovision Finale", color=embedColour)

        await ctx.send(embed=embed)

#    @commands.command()
#    async def enter(self, ctx):
#        """Enters you to the voting able list"""
#        with pointsfile.as_dict(write=True) as data:
#            if str(ctx.message.author.id) in data:
#                await ctx.message.add_reaction("✅")
#                #await ctx.send("You have already entered.")
#            else:
#                data.update({str(ctx.message.author.id): {}})
#                await ctx.message.add_reaction("✅")
#                #await ctx.send("We have recieved your entry.")

    @commands.command()
    async def song(self, ctx, country_song_number):
        """Shows information about the <song>"""
        if ctx.guild == None:
            name = None
        else:
            name = ctx.guild.name
        log(ctx.message.created_at, ctx.message.author, ctx.message.content, name, ctx.message.channel, logfile)
        if episode == "beta_sf1":
            countryIndex = sf1
        elif episode == "sf1":
            countryIndex = sf1
        elif episode == "sf2":
            countryIndex = sf2
        elif episode == "finale":
            countryIndex = finale

        with countries.as_dict(write=False) as data:
            countryList = data
        esc = []
        if episode == "beta_sf1":
            countryIndex = sf1
            for ctry in sf1:
                esc.append(sf1[ctry])
        elif episode == "sf1":
            countryIndex = sf1
            for ctry in sf1:
                esc.append(sf1[ctry])
        elif episode == "sf2":
            countryIndex = sf2
            for ctry in sf2:
                esc.append(sf2[ctry])
        elif episode == "finale":
            countryIndex = finale
            for ctry in finale:
                esc.append(finale[ctry])

        if country_song_number in esc:
            embed = discord.Embed(title="Eurovision "+year, color=embedColour)
            land = country_song_number
            artist = countryList[country_song_number]["artist"]
            song = countryList[country_song_number]["song"]
            lang = countryList[country_song_number]["lang"]
            txt = f"{land} "
            txt_sang = f"{song} sung by {artist} in {lang}"
            embed.add_field(name=txt, value=txt_sang, inline=False)
            await ctx.send(embed=embed)
        elif country_song_number.isnumeric():
            if 0 < int(country_song_number) <= len(countryIndex):
                if episode == "beta_sf1":
                    embed = discord.Embed(title="BETA: Eurovision 1st Semi-final", color=embedColour)
                elif episode == "sf1":
                    embed = discord.Embed(title="Eurovision 1st Semi-final", color=embedColour)
                elif episode == "sf2":
                    embed = discord.Embed(title="Eurovision 2nd Semi-final", color=embedColour)
                elif episode == "finale":
                    embed = discord.Embed(title="Eurovision Finale", color=embedColour)

                land = countryIndex[int(country_song_number)]
                artist = countryList[countryIndex[int(country_song_number)]]["artist"]
                song = countryList[countryIndex[int(country_song_number)]]["song"]
                lang = countryList[countryIndex[int(country_song_number)]]["lang"]
                txt = f"**{country_song_number}** {land} "
                txt_sang = f"{song} sung by {artist} in {lang}"
                embed.add_field(name=txt, value=txt_sang, inline=False)
                await ctx.send(embed=embed)
            else:
                await ctx.message.add_reaction("❌")
                #await ctx.send("This country is not in this episode. Try " + str(countryIndex) + " instead.")
        else:
            await ctx.message.add_reaction("❌")
            #await ctx.send("This country is not in this episode. Try " + str(countryIndex) + " instead.")

    @commands.command()
    async def stream(self, ctx):
        """Streams"""
        if ctx.guild == None:
            name = None
        else:
            name = ctx.guild.name
        log(ctx.message.created_at, ctx.message.author, ctx.message.content, name, ctx.message.channel, logfile)
        embed = discord.Embed(title="Eurovision "+year, color=embedColour)
        txt="Official YouTube Stream"
        url=youtubelink
        embed.add_field(name=txt, value=url, inline=False)
        txt="Norwegian Stream"
        url="https://tv.nrk.no/direkte/nrk1"
        embed.add_field(name=txt, value=url, inline=False)
        txt="Swedish Stream"
        url="https://svtplay.se/kanaler/svt1"
        embed.add_field(name=txt, value=url, inline=False)
        txt="Danish Stream"
        url="https://dr.dk/drtv/kanal/dr1_20875"
        embed.add_field(name=txt, value=url, inline=False)
        await ctx.send(embed=embed)

    @commands.command()
    async def botinfo(self, ctx):
        """Information about this bot"""
        if ctx.guild == None:
            name = None
        else:
            name = ctx.guild.name
        log(ctx.message.created_at, ctx.message.author, ctx.message.content, name, ctx.message.channel, logfile)
        #avatar = self.bot.user.avatar_url_as(format=None, static_format='png', size=1024)
        repo = discord.Embed(color=embedColour)
        repo.set_author(name=self.bot.user.name)#, icon_url=avatar)
        #repo.set_thumbnail(url=avatar)
        repo.add_field(
            name="What?", value="A bot made by <@202745416062599168> and <@142212883512557569> for counting Eurovision votes.")
        repo.add_field(
            name="Source", value="[Gitlab](https://gitlab.com/MarlinMr/eurovision-bot).", inline=True)
        await ctx.send(embed=repo)

    @commands.command()
    async def ip(self,ctx):
        """Skriver lokal IP"""
        if ctx.message.author.id == 202745416062599168:
            my_ip = get('https://api.ipify.org').content.decode('utf8')
        else:
            await ctx.message.add_reaction("❌")
        await ctx.send("`"+my_ip+"`")


async def setup(bot):
    n = eurovision(bot)
    await bot.add_cog(n)

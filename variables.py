episode = "finale"
youtubelink = "https://www.youtube.com/watch?v=KSYE-C1TuXo"
sf1 = {
     1: '🇨🇾',
     2: '🇷🇸',
     3: '🇱🇹',
     4: '🇮🇪',
     5: '🇺🇦',
     6: '🇵🇱',
     7: '🇭🇷',
     8: '🇮🇸',
     9: '🇸🇮',
    10: '🇫🇮',
    11: '🇲🇩',
    12: '🇦🇿',
    13: '🇦🇺',
    14: '🇵🇹',
    15: '🇱🇺'
}
sf2 = {
     1: '🇲🇹',
     2: '🇦🇱',
     3: '🇬🇷',
     4: '🇨🇭',
     5: '🇨🇿',
     6: '🇦🇹',
     7: '🇩🇰',
     8: '🇦🇲',
     9: '🇱🇻',
    10: '🇸🇲',
    11: '🇬🇪',
    12: '🇧🇪',
    13: '🇪🇪',
    14: '🇮🇱',
    15: '🇳🇴',
    16: '🇳🇱'
}
finale = {
     1: '🇸🇪',
     2: '🇺🇦',
     3: '🇩🇪',
     4: '🇱🇺',
     5: '🇳🇱',
     6: '🇮🇱',
     7: '🇱🇹',
     8: '🇪🇸',
     9: '🇪🇪',
    10: '🇮🇪',
    11: '🇱🇻',
    12: '🇬🇷',
    13: '🇬🇧',
    14: '🇳🇴',
    15: '🇮🇹',
    16: '🇷🇸',
    17: '🇫🇮',
    18: '🇵🇹',
    19: '🇦🇲',
    20: '🇨🇾',
    21: '🇨🇭',
    22: '🇸🇮',
    23: '🇭🇷',
    24: '🇬🇪',
    25: '🇫🇷',
    26: '🇦🇹'
}
country_names = {
    '🇦🇱':["al","albania"],
    '🇦🇲':["am","arm","armenia"],
    '🇦🇺':["au","aus","australia"],
    '🇦🇹':["at","austria","østerriket"],
    '🇦🇿':["az","azerbaijan","aze"],
    '🇧🇪':["be","bel","belgium","belgia"],
    '🇨🇭':["ch","che","switzerland","sveits"],
    '🇨🇾':["cy","cyprus"],
    '🇨🇿':["cz","chz","czech","czechia","tsjekkia"],
    '🇩🇪':["de","ger","germany","tyskland"],
    '🇩🇰':["dk","den","denmark","danmark"],
    '🇪🇪':["ee","est","estonia","estland"],
    '🇪🇸':["es","esp","spania","espania","spain"],
    '🇫🇮':["fi","fin","finland","suomi"],
    '🇫🇷':["fr","fra","france","frankrike"],
    '🇬🇷':["gr","grd","greece","hellas"],
    '🇬🇪':["ge","georgia"],
    '🇭🇷':["hr","croatia"],
    '🇮🇱':["il","israel"],
    '🇮🇹':["it","ita","italy","italia"],
    '🇮🇸':["is","isl","island","iceland"],
    '🇮🇪':["ie","ire","ireland"],
    '🇱🇹':["lt","lithuania","ltu","litauen"],
    '🇱🇻':["lv","latvia"],
    '🇱🇺':["lu","luxembourg"],
    '🇲🇩':["md","mda","moldova"],
    '🇲🇹':["mt","malta"],
    '🇳🇱':["nl","nld","nederland","netherlands"],
    '🇳🇴':["no","nor","norge","norway"],
    '🇵🇱':["pl","pol","poland","polen"],
    '🇵🇹':["pt","prt","portugal"],
    '🇸🇪':["se","swe","sweden","sverige"],
    '🇸🇮':["si","slovenia", "slovenien"],
    '🇸🇲':["sm","san marino","marino","san"],
    '🇷🇴':["ro","rom","romania"],
    '🇷🇸':["rs","serbia","srb"],
    '🇺🇦':["ua","ukr","ukraine","ukraina","ru","rus","russia","russland"],
    '🇬🇧':["uk","gbr","gb","england","storbritannia"]
}

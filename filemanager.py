from contextlib import contextmanager
from json.decoder import JSONDecodeError
import os
import json
import pathlib


class FileLoaderBase:
    def __init__(self, path) -> None:
        self.file_path = pathlib.Path(path).expanduser()
        self.folder = self.file_path.parents[0]

        if not os.path.exists(self.folder):
            os.makedirs(self.folder)

        if not os.path.exists(self.file_path):
            with open(self.file_path, 'w+') as f:
                if (self.empty_file):
                    f.write(self.empty_file)
        pass


class FileLoader(FileLoaderBase):
    def __init__(self, path) -> None:
        self.empty_file = ""
        super().__init__(path)

    @contextmanager
    def as_file(self, mode: str, encoding: str = 'utf8'):
        try:
            file = open(self.file_path, mode, encoding=encoding)
            yield file
        finally:
            file.close()

class JsonLoader(FileLoaderBase):
    def __init__(self, path) -> None:
        self.empty_file = "{}"
        self.json = None
        super().__init__(path)

    @contextmanager
    def as_dict(self, write: bool = True, encoding: str = 'utf8'):
        mode = 'r+' if write else 'r'
        decodeError = False
        try:
            file = open(self.file_path, mode, encoding=encoding)
            self.json = json.load(file)
            yield self.json
        except JSONDecodeError as e:
            raise e
        finally:
            if write and not decodeError:
                file.seek(0)
                json.dump(self.json, file)
                file.truncate()
            file.close()
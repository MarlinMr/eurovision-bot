#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import discord
import sys
import asyncio
from discord.ext import commands
from os.path import abspath, expanduser
import filemanager
sys.dont_write_bytecode = True

settingsfile = filemanager.JsonLoader('/home/marlinmr/src/marlinmr/eurovision-bot/config/discordConfig.json')

with settingsfile.as_dict(write=False, encoding='utf8') as data:
    token = data["token"]
    prefix = data["prefix"]
    status = data["playing"]

intents = discord.Intents.default()
intents.message_content = True

bot = commands.Bot(command_prefix=prefix, prefix=prefix, case_insensitive=True, intents=intents)
print("Loading cogs...")

async def load():
    for guild in bot.guilds:
        print(f'Logged in as: {bot.user.name} in {guild.name}. Version: {discord.__version__}')
    for file in os.listdir(abspath(expanduser('./cogs'))):
        if (file.endswith(".py")):
            name = file[:-3]
            await bot.load_extension(f"cogs.{name}")

async def main():
    print("Starting bot...")
    await load()
    await bot.start(token, reconnect=True)

asyncio.run(main())
